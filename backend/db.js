const mysql = require("mysql2");

const openConnection = () => {
  const connection = mysql.createConnection({
    port: 3306,
    host: "db",
    user: "root",
    password: "root",
    database: "db",
  });

  connection.connect();

  return connection;
};

module.exports = {
  openConnection,
};
