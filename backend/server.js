const express = require("express");
const db = require("./db");
const utils = require("./utils");
const cors = require("cors");

const app = express();

app.use(cors("*"));

app.use(express.json());

app.get("/", (request, response) => {
  console.log("In get");
  const statement = `select * from emp `;
  const connection = db.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

app.post("/", (request, response) => {
  console.log("In post");
  const { name, salary, age } = request.body;

  const statement = `
      insert into emp
        ( name, salary, age)
      values 
        ( '${name}', '${salary}', '${age}')
    `;

  const connection = db.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

app.put("/:id", (request, response) => {
  console.log("In put");
  const { salary } = request.body;
  const { id } = request.params;

  const statement = `
      update emp
      set 
        
        salary = '${salary}' 
       
      where
        empid = ${id}
    `;

  const connection = db.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

app.delete("/:id", (request, response) => {
  console.log("In delete");
  const { id } = request.params;

  const statement = `
      delete from emp
      where
        empid = ${id}
    `;

  const connection = db.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

app.listen(4000, "0.0.0.0", () => {
  console.log("server started on port 4000");
});
